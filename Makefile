.PHONY: test new

test:
	@echo "Running CMake to configure the project..."
	cmake -S . -B build -Wno-dev
	@echo "Building the project..."
	cmake --build build
	@echo "Running tests..."
	cd build && ctest

new:
	@echo "Creating a new file with the specified number in the src directory..."
	@if [ -z "$(number)" ]; then \
		echo "Error: number parameter is required. Use 'make new number=YourNumber'"; \
		exit 1; \
	fi
	@mkdir -p src
	@echo '#include <gtest/gtest.h>' > src/problem$(number).cc
	@echo >> src/problem$(number).cc
	@echo '#include <algorithm>' >> src/problem$(number).cc
	@echo '#include <iostream>' >> src/problem$(number).cc
	@echo '#include <unordered_map>' >> src/problem$(number).cc
	@echo '#include <vector>' >> src/problem$(number).cc
	@echo >> src/problem$(number).cc
	@echo 'using namespace std;' >> src/problem$(number).cc
	@echo >> src/problem$(number).cc
	@echo 'class Solution {' >> src/problem$(number).cc
	@echo 'public:' >> src/problem$(number).cc
	@echo '    int xxxx(vector<int>& nums) {' >> src/problem$(number).cc
	@echo '        ' >> src/problem$(number).cc
	@echo '    }' >> src/problem$(number).cc
	@echo '};' >> src/problem$(number).cc
	@echo >> src/problem$(number).cc
	@echo 'TEST(Test$(number), unit_test) {' >> src/problem$(number).cc
	@echo '  Solution c;' >> src/problem$(number).cc
	@echo '  vector<int> nums1 = {1, 2, 3, 3};' >> src/problem$(number).cc
	@echo '  EXPECT_EQ(c.xxxx(nums1), 3);' >> src/problem$(number).cc
	@echo '}' >> src/problem$(number).cc
	@echo "File src/problem$(number).cc has been created successfully."
