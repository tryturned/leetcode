#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  bool canJump(vector<int>& nums) {
    int mx = 0;
    for (int i = 0; i < nums.size(); i++) {
      if (mx < i) return false;
      mx = max(mx, i + nums[i]);
      mx = max(mx, i + nums[i]);
    }
    return true;
  }
};

TEST(Test55, unit_test) {
  Solution c;
  vector<int> nums1 = {2, 3, 1, 1, 4};
  EXPECT_EQ(c.canJump(nums1), true);

  vector<int> nums2 = {3, 2, 1, 0, 4};
  EXPECT_EQ(c.canJump(nums1), true);
}
