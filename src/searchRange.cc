#include <gtest/gtest.h>

#include <vector>

using namespace std;

class Solution {
 private:
  int findFirst(vector<int>& nums, int target) {
    int l = 0, r = nums.size() - 1;
    while (l <= r) {
      int mid = l + (r - l) / 2;
      if (nums[mid] >= target) {
        r = mid - 1;
      } else {
        l = mid + 1;
      }
    }
    return l < nums.size() && nums[l] == target ? l : -1;
  }

  int findLast(vector<int>& nums, int target) {
    int l = 0, r = nums.size() - 1;
    while (l <= r) {
      int mid = l + (r - l) / 2;
      if (nums[mid] <= target) {
        l = mid + 1;
      } else {
        r = mid - 1;
      }
    }
    return r < nums.size() && nums[r] == target ? r : -1;
  }

 public:
  vector<int> searchRange(vector<int>& nums, int target) {
    if (nums.empty()) return {-1, -1};
    return {findFirst(nums, target), findLast(nums, target)};
  }
};

TEST(BinarySearchTest, searchRange) {
  Solution sol;

  vector<int> input = vector<int>{5, 7, 7, 8, 8, 10};
  vector<int> except1 = vector<int>{3, 4};
  EXPECT_EQ(except1, sol.searchRange(input, 8));

  vector<int> except2 = vector<int>{0, 0};
  EXPECT_EQ(except2, sol.searchRange(input, 5));

  vector<int> except3 = vector<int>{-1, -1};
  EXPECT_EQ(except3, sol.searchRange(input, 6));

  vector<int> input3 = vector<int>{};
  EXPECT_EQ(except3, sol.searchRange(input3, 6));

  vector<int> input4 = vector<int>{1};
  EXPECT_EQ(except2, sol.searchRange(input4, 1));
  EXPECT_EQ(except3, sol.searchRange(input4, 3));
}
