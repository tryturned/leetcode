#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<string> subdomainVisits(vector<string>& cpdomains) {
    // 1. 校验cpdomains的输入长度，以及校验每个字符串的合法性，确保格式为 ^num a.b.c$ 格式 -》跳过
    // 2. 用 hashmap 计数累加
    unordered_map<string, int> subdomainCnt;
    for (auto cpdomain : cpdomains) {
      size_t blankIdx = cpdomain.find(' ');
      int num = stoi(cpdomain.substr(0, blankIdx));

      string domainStr = cpdomain.substr(blankIdx + 1);
      while (!domainStr.empty()) {
        subdomainCnt[domainStr] += num;
        size_t firstDot = domainStr.find('.');
        if (firstDot == string::npos) {
          domainStr = "";
        } else {
          domainStr = domainStr.substr(firstDot + 1);
        }
      }
    }

    vector<string> result;
    for (auto kv : subdomainCnt) {
      result.push_back(to_string(kv.second) + " " + kv.first);
    }
    return result;
  }
};

TEST(DomainTest, subdomainVisits) {
  Solution sol;

  vector<string> case1 = {"9001 discuss.leetcode.com"};
  vector<string> expect1 = {"9001 leetcode.com", "9001 discuss.leetcode.com", "9001 com"};
  auto result1 = sol.subdomainVisits(case1);
  sort(expect1.begin(), expect1.end());
  sort(result1.begin(), result1.end());
  EXPECT_EQ(expect1, result1);

  vector<string> case2 = {"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"};
  vector<string> expect2 = {"901 mail.com",     "50 yahoo.com", "900 google.mail.com", "5 wiki.org", "5 org",
                            "1 intel.mail.com", "951 com"};
  auto result2 = sol.subdomainVisits(case2);
  sort(expect2.begin(), expect2.end());
  sort(result2.begin(), result2.end());
  EXPECT_EQ(expect2, result2);

  vector<string> case3 = {"9001 com"};
  vector<string> expect3 = {"9001 com"};
  auto result3 = sol.subdomainVisits(case3);
  EXPECT_EQ(expect3, result3);
}
