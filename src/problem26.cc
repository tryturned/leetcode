#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int removeDuplicates(vector<int>& nums) {
    int idx = 0;
    for (int i = 1; i < nums.size(); i++) {
      if (nums[i] != nums[idx]) {
        nums[++idx] = nums[i];
      }
    }
    return idx + 1;
  }
};

TEST(Test26, test) {
  Solution c;
  vector<int> nums1 = {1, 2, 3, 3};
  EXPECT_EQ(c.removeDuplicates(nums1), 3);
}