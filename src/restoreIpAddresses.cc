#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
  vector<string> result;
  void backtrack(string s, string cur, int start, int partion) {
    if (partion > 4) return;
    if (s.size() == start && partion == 4) {
      result.push_back(cur.substr(1));
      return;
    }
    if (s[start] == '0') {
      backtrack(s, cur + '.' + s[start], start + 1, partion + 1);
      return;
    }
    int subIp = 0;
    for (int i = start; i < s.size(); i++) {
      subIp = subIp * 10 + (s[i] - '0');
      if (subIp > 255) {
        break;
      }
      backtrack(s, cur + "." + to_string(subIp), i + 1, partion + 1);
    }
  }

 public:
  vector<string> restoreIpAddresses(string s) {
    result = {};
    backtrack(s, "", 0, 0);
    return result;
  }
};

TEST(BacktrackTest, restoreIpAddresses) {
  Solution c;
  vector<string> expect1{"255.255.11.135", "255.255.111.35"};
  vector<string> result1 = c.restoreIpAddresses("25525511135");
  sort(expect1.begin(), expect1.end());
  sort(result1.begin(), result1.end());
  EXPECT_EQ(expect1, result1);
}
