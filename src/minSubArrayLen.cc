#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

int minSubArrayLen(int target, vector<int>& nums) {
  int res = INT32_MAX;
  int windowSum = 0, n = nums.size();
  int start = 0;
  for (int i = 0; i < n; i++) {
    windowSum += nums[i];
    while (windowSum >= target) {
      res = min(res, i - start + 1);
      windowSum -= nums[start++];
    }
  }
  return res == INT_MAX ? 0 : res;
}

TEST(WindowTest, minSubArrayLen) {
  vector<int> input1{2, 3, 1, 2, 4, 3};
  EXPECT_EQ(minSubArrayLen(7, input1), 2);

  vector<int> input2{1, 4, 4};
  EXPECT_EQ(minSubArrayLen(4, input2), 1);
  EXPECT_EQ(minSubArrayLen(9, input2), 3);
  EXPECT_EQ(minSubArrayLen(90, input2), 0);
}
