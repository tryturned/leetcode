#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
    if (n <= 0) return;
    int i = m - 1, j = n - 1;
    for (int idx = m + n - 1; idx >= 0; idx--) {
      if (i >= 0 && j >= 0) {
        if (nums1[i] >= nums2[j]) {
          nums1[idx] = nums1[i--];
        } else {
          nums1[idx] = nums2[j--];
        }
      } else if (i >= 0) {
        nums1[idx] = nums1[i--];
      } else {
        nums1[idx] = nums2[j--];
      }
    }
  }
};

TEST(Test88, merge) {
  Solution c;
  vector<int> nums1 = {1}, nums2 = {};
  c.merge(nums1, 1, nums2, 0);
  vector<int> expect1 = {1};
  EXPECT_EQ(expect1, nums1);
}