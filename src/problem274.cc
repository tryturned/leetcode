#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int hIndex(vector<int>& citations) {
    sort(citations.begin(), citations.end(), [](int a, int b) { return a > b; });
    for (int h = citations.size(); h > 0; h--) {
      if (citations[h - 1] >= h) {
        return h;
      }
    }
    return 0;
  }
};

TEST(Test274, unit_test) {
  Solution c;
  vector<int> nums1 = {3, 0, 6, 1, 5};
  EXPECT_EQ(c.hIndex(nums1), 3);
}
