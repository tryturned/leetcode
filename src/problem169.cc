#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int majorityElement(vector<int>& nums) {
    int num = 0, occur = 0;
    for (int val : nums) {
      if (occur == 0) {
        num = val;
        occur++;
      } else {
        occur += num == val ? 1 : -1;
      }
    }
    return num;
  }
};

TEST(Test169, unit_test) {
  Solution c;
  vector<int> nums1 = {1, 3, 3, 3};
  EXPECT_EQ(c.majorityElement(nums1), 3);
}
