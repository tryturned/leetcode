#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class RandomizedSet {
 public:
  RandomizedSet() {}

  bool insert(int val) {
    if (idxMap.count(val)) return false;
    idxMap[val] = elems.size();
    elems.push_back(val);
    return true;
  }

  bool remove(int val) {
    if (!idxMap.count(val)) return false;
    size_t rawIdx = idxMap[val];
    elems[rawIdx] = elems.back();
    idxMap[elems.back()] = rawIdx;
    idxMap.erase(val);
    elems.pop_back();
    return true;
  }

  int getRandom() { return elems[rand() % elems.size()]; }

 private:
  unordered_map<int, size_t> idxMap;

  vector<int> elems;
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */
