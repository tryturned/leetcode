#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int jump(vector<int>& nums) {
    vector<int> dp(nums.size(), INT_MAX);
    dp[0] = 0;
    for (int i = 1; i < nums.size(); i++) {
      for (int j = 0; j < i; j++) {
        if (j + nums[j] >= i) {
          dp[i] = min(dp[i], dp[j] + 1);
        }
      }
    }
    return dp[nums.size() - 1];
  }
};

TEST(Test45, unit_test) {
  Solution c;
  vector<int> nums1 = {2, 3, 1, 1, 4};
  EXPECT_EQ(c.jump(nums1), 2);
}
