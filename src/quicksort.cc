#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

void quicksort(vector<int>& nums, int start, int end) {
  if (start >= end) return;
  int l = start, r = end, mid = l + (r - l) / 2;
  int pivot = nums[mid];
  swap(nums[mid], nums[end]);
  int i = start;
  for (int j = start; j < end; j++) {
    if (nums[j] < pivot) {
      swap(nums[i++], nums[j]);
    }
  }
  swap(nums[i], nums[end]);

  quicksort(nums, start, i - 1);
  quicksort(nums, i + 1, end);
}

// I love quick sort
vector<int> sortArray(vector<int>& nums) {
  quicksort(nums, 0, nums.size() - 1);
  return nums;
}

TEST(SortTest, sortArray) {
  vector<int> input1{2, 3, 1, 2, 4, 3};
  vector<int> expect1{1, 2, 2, 3, 3, 4};
  EXPECT_EQ(sortArray(input1), expect1);

  vector<int> input2{1, 4, 4};
  vector<int> expect2{1, 4, 4};
  EXPECT_EQ(sortArray(input2), expect2);
}
