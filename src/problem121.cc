#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int maxProfit(vector<int>& prices) {
    int cost = prices[0], profit = 0;
    for (int i = 1; i < prices.size(); i++) {
      if (prices[i] > cost) {
        profit = max(profit, prices[i] - cost);
      }
      cost = min(cost, prices[i]);
    }
    return profit;
  }
};

TEST(Test121, unit_test) {
  Solution c;
  vector<int> nums1 = {7, 1, 5, 3, 6, 4};
  EXPECT_EQ(c.maxProfit(nums1), 5);
}
