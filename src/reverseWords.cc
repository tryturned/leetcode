#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  string reverseWords(string s) {
    reverse(s.begin(), s.end());
    string result;
    for (int i = 0; i < s.size(); i++) {
      if (s[i] == ' ') continue;
      int l = i, r = i;
      while (r < s.size() && s[r] != ' ') {
        r++;
      }
      string substr = s.substr(l, r - l);
      reverse(substr.begin(), substr.end());
      result += (result.empty() ? "" : " ") + substr;
      i = r;
    }
    return result;
  }
};

TEST(TwoPointerTest, reverseWords) {
  Solution c;
  EXPECT_EQ(c.reverseWords("25525511135"), "25525511135");

  EXPECT_EQ(c.reverseWords(" 25   5 25   5  "), "5 25 5 25");

  EXPECT_EQ(c.reverseWords(" 25   "), "25");
}
