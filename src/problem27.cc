#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int removeElement(vector<int>& nums, int val) {
    int idx = 0;
    for (int i = 0; i < nums.size(); i++) {
      if (nums[i] != val) {
        nums[idx++] = nums[i];
      }
    }
    return idx;
  }
};

TEST(Test27, merge) {
  Solution c;
  vector<int> nums1 = {1, 2, 3};
  EXPECT_EQ(c.removeElement(nums1, 1), 2);
}