#include <gtest/gtest.h>

#include <algorithm>
#include <deque>
#include <iostream>
#include <stack>
#include <unordered_map>
#include <vector>

using namespace std;

vector<string> splitStr(string& s, char delimter) {
  vector<string> result;
  istringstream tokenStream(s);
  string token;
  while (getline(tokenStream, token, delimter)) {
    if (token == "" || token == ".") continue;
    result.push_back(token);
  }
  return result;
}

string simplifyPath(string path) {
  deque<string> pathStack;
  auto pathParams = splitStr(path, '/');
  for (auto param : pathParams) {
    if (param == "..") {
      if (!pathStack.empty()) pathStack.pop_back();
    } else {
      pathStack.push_back(param);
    }
  }
  string result;
  for (auto& str : pathStack) {
    result += "/" + str;
  }
  return result.empty() ? "/" : result;
}

TEST(StackTest, simplifyPath) {
  EXPECT_EQ(simplifyPath("/sss/../..././dsd/"), "/.../dsd");

  EXPECT_EQ(simplifyPath("/home//foo/"), "/home/foo");

  EXPECT_EQ(simplifyPath("/../"), "/");
}
