#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int maxProfit2(vector<int>& prices) {
    int res = 0;
    for (int i = 1; i < prices.size(); ++i) {
      if (prices[i] > prices[i - 1]) {
        res += prices[i] - prices[i - 1];
      }
    }
    return res;
  }
};

TEST(Test122, unit_test) {
  Solution c;
  vector<int> nums1 = {7, 1, 5, 3, 6, 4};
  EXPECT_EQ(c.maxProfit2(nums1), 7);
}
