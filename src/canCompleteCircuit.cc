#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
    int start = 0, n = gas.size();
    while (start < n) {
      int cnt = 0, sum = 0;
      while (cnt < n) {
        int end = (start + cnt) % n;
        sum += gas[end] - cost[end];
        if (sum < 0) {
          break;
        }
        cnt++;
      }
      if (cnt == n) {
        return start;
      } else {
        start += cnt + 1;
      }
    }
    return -1;
  }
};

TEST(TwoPointerTest, canCompleteCircuit) {
  Solution c;
  vector<int> gas1{1, 2, 3, 4, 5};
  vector<int> cost1{3, 4, 5, 1, 2};
  EXPECT_EQ(c.canCompleteCircuit(gas1, cost1), 3);

  vector<int> gas2{1, 2, 3};
  vector<int> cost2{1, 4, 5};
  EXPECT_EQ(c.canCompleteCircuit(gas2, cost2), -1);

  vector<int> gas3{4, 3, 4};
  vector<int> cost3{3, 4, 3};
  EXPECT_EQ(c.canCompleteCircuit(gas3, cost3), 0);

  vector<int> gas4{4};
  vector<int> cost4{4};
  EXPECT_EQ(c.canCompleteCircuit(gas4, cost4), 0);
}
