#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
  vector<vector<int>> res;
  int start = newInterval[0], end = newInterval[1];
  bool inserted = false;

  for (auto pair : intervals) {
    int s = pair[0], e = pair[1];
    if (s > end) {
      if (!inserted) {
        res.push_back({start, end});
        inserted = true;
      }
      res.push_back(pair);
    } else if (e < start) {
      res.push_back(pair);
    } else {
      start = min(start, s);
      end = max(end, e);
    }
  }
  if (!inserted) {
    res.push_back({start, end});
  }
  return res;
}

TEST(PartitionTest, insertedAndMerge) {
  vector<vector<int>> input1_1{{2, 3}, {4, 8}};
  vector<int> input1_2{1, 2};
  vector<vector<int>> expect1{{1, 3}, {4, 8}};
  EXPECT_EQ(insert(input1_1, input1_2), expect1);

  vector<vector<int>> input2_1{{2, 4}, {6, 8}};
  vector<int> input2_2{2, 7};
  vector<vector<int>> expect2{{2, 8}};
  EXPECT_EQ(insert(input2_1, input2_2), expect2);
}
