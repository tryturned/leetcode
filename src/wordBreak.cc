#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  bool wordBreak(string s, vector<string>& wordDict) {
    int len = s.length(), wordNum = wordDict.size();
    vector<bool> dp(len + 1, false);
    dp[0] = true;
    for (int i = 1; i <= len; i++) {
      for (int j = 0; j < wordNum; j++) {
        if (dp[i]) break;
        int substrLen = wordDict[j].length();
        if (substrLen <= i && dp[i - substrLen]) {
          dp[i] = s.substr(i - substrLen, substrLen) == wordDict[j];
        }
      }
    }
    return dp[len];
  }
};

TEST(DynamicTest, wordBreak) {
  Solution c;

  vector<string> wordDict1{"leet", "code"};
  EXPECT_EQ(c.wordBreak("leetcode", wordDict1), true);

  vector<string> wordDict2{"apple", "pen"};
  EXPECT_EQ(c.wordBreak("applepenapple", wordDict2), true);

  vector<string> wordDict3{"cats", "dog", "sand", "and", "cat"};
  EXPECT_EQ(c.wordBreak("catandg", wordDict3), false);
}
